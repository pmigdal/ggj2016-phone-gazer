﻿using UnityEngine;
using System.Collections;

public class RandomFromDistribution
{
	// probabilities do not need to be nornalized
	public static int Integer (float[] probabilities)
	{
		float total = 0f;
		foreach (float prob in probabilities) {
			total += prob;
		}
		float rand = Random.Range (0f, total);
		int i = 0;
		float cumSum = 0f;
		foreach (float prob in probabilities) {
			cumSum += prob;
			if (cumSum > rand) {
				return i;
			}
			i++;
		}
		return -1;
	}
}
