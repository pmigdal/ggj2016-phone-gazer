﻿using UnityEngine;
using System.Collections;

public class RandomPost
{

	public string kind;
	public string text;
	public int valence;

	public static string[] beings = new string[] {
		"mother", "gerbil", "boyfriend", "girlfriend", "new Mac Book Pro", "kitty", "puppy", "dog", "Max",
		"iPhone", "mate", "honey", "teacher", "Cloe", "imaginary friend", "wild animal", "Adam", "Kate",
		"iPhone charger", "selfie stick", "git account"
	};

	public static string[] time = new string[] {
		"yesterday", "15 min ago", "a year ago", "somehow", "unexpectedly", "a few secs ago",
		"last friday", "last week", "i don't know when", "this coffee break", "when taking a shower", "at home"
	};

	public static string[] causeOfDeath = new string[] {
		"in a violent crash", "in a car accident", "because of cancer", "- it was a suicide", "burnt alive",
		"- she jumped of the roof", "(slipped on a banana peel)", "- it ate a toxic cucumber", "(swallowed a balloon)",
		"(abortion complications)", "in a kniting accident", "because of broken wifi connection", "no Internet for 3 days",
		"while taking a selfie", "with no friends", "because of drinking decaf" 
	};

	public static string[] adjPositive = new string[] {
		"funny", "good", "awesome", "lovely", "charming", "cooool",
		"vert best", "tasty", "ok", "cute", "nice", "fcking favourite"
	};

	public RandomPost ()
	{
		if (Random.Range (0, 3) == 0) {
			PostDied ();
		} else {
			PostGood ();
		}
	}

	string chooseRandom (string[] names) {
		int rand = Random.Range (0, names.Length);
		return names [rand];
	}

	void PostDied () {

		text = string.Format("My {0} died {1} {2}! :/",
			chooseRandom(beings),
			chooseRandom(time),
			chooseRandom(causeOfDeath)
		);
		kind = "died";
		valence = -5;
	}

	void PostGood () {

		kind = "good";
		valence = 3;
		text = string.Format("My {0} is sooo {1}!",
			chooseRandom(beings),
			chooseRandom(adjPositive)
		);
	}
}
