﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBehaviour : MonoBehaviour {
	public static UIBehaviour instance;
	public Text txtSocialMeter, txtDistanceMeter;
	public AudioSource sndBad,sndGood;

	int _socialMeter;
	public int socialMeter {
		get { return _socialMeter; }
		set {
			_socialMeter = value;
			txtSocialMeter.text = string.Format("FRIENDS: {0}", value);
			if (value < 0) {
				PlayerBehaviour.instance.Die();
			}
		}
	}

	int _distanceMeter;
	public int distanceMeter {
		get { return _distanceMeter; }
		set {
			_distanceMeter = value;
			txtDistanceMeter.text = string.Format("{0} M", value);
		}
	}

	// Use this for initialization
	void Start () {
		instance = this;
		socialMeter = 25;
		distanceMeter = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnNotificationsClick() {
		NotificationsTabBehaviour.instance.scrollRect.gameObject.SetActive(true);
		FeedTabBehaviour.instance.scrollRect.gameObject.SetActive(false);
	}
	public void OnFeedPostsClick() {
		NotificationsTabBehaviour.instance.scrollRect.gameObject.SetActive(false);
		FeedTabBehaviour.instance.scrollRect.gameObject.SetActive(true);
	}

	public void OnRestartClick() {
		Application.LoadLevel(Application.loadedLevel);
	}
}
