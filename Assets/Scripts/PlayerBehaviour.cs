﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {
	const float HEAD_BASE_X_ROT = 35.0f;
	const float HEAD_RADIUS = 5.0f;
	const float LANE_SWITCH_SPEED = 8.0f;

	public static PlayerBehaviour instance;
	Transform t;
	public Transform cam;
	public Animator anim;
	public GameObject deathScreen;
	public AudioSource sndDeath;
	int lane = 0;

	// Use this for initialization
	void Start () {
		instance = this;
		t = transform;
	}
	
	// Update is called once per frame
	void Update () {
		float xPos = Mathf.Lerp(t.localPosition.x,lane * LevelBehaviour.ONE_UNIT_Z,Time.deltaTime * LANE_SWITCH_SPEED);
		t.localPosition = new Vector3(xPos,0,0);
//		cam.localRotation = Quaternion.Euler(HEAD_BASE_X_ROT + Mathf.Sin(Time.time * 2.0f) * HEAD_RADIUS,0,0);
//		print (lane);
		if (Input.GetKeyDown(KeyCode.A)) {
			lane = Mathf.Max (0,lane-1);
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			lane = Mathf.Min(LevelBehaviour.instance.laneCount-1, lane+1);
		}
	}

	public void Die() {
		enabled = false;
		LevelBehaviour.instance.enabled = false;
		anim.SetTrigger("Dead");
		anim.SetBool("HeadDown", false);
		HandBehaviour.instance.handContainerAnim.SetBool("Visible", false);
		HandBehaviour.instance.enabled = false;
		deathScreen.SetActive(true);
		sndDeath.Play();
	}

	void OnTriggerEnter(Collider coll) {
		Die();
	}
}
