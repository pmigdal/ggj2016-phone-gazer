﻿using UnityEngine;
using System.Collections;

public class HandBehaviour : MonoBehaviour {
	const float Y_HIDE_MARGIN = -250.0f;
	const float HAND_SPEED = 10.0f;
	public static HandBehaviour instance;
	public Camera UICamera;
	public Animator handContainerAnim;
	RectTransform t;
	// Use this for initialization
	void Start () {
		instance = this;
		t = transform as RectTransform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 vTmp = (Vector2)UICamera.ScreenToViewportPoint(Input.mousePosition) - Vector2.one * 0.5f;
		vTmp.x *= 800.0f;
		vTmp.y *= 600.0f;
		t.anchoredPosition = Vector2.Lerp(t.anchoredPosition,vTmp, HAND_SPEED * Time.deltaTime);

		if (PlayerBehaviour.instance) {
			bool phoneVisible = t.anchoredPosition.y > Y_HIDE_MARGIN;
			PlayerBehaviour.instance.anim.SetBool("HeadDown", phoneVisible);
			handContainerAnim.SetBool("Visible", phoneVisible);
		}
	}
}
