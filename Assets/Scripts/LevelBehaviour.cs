﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// using UnityEngine.UI;

public class LevelBehaviour : MonoBehaviour {
	//public const float LANE_DISTANCES = 2.77f;
	//public const float INITIAL_OBSTACLE_Z = 4.78f;
	public const float MOVEMENT_SPEED = 2.0f;
	public const float ONE_UNIT_Z = 1.0f;

	const int MIN_Z_DISTANCE = 3;
	const int MAX_Z_DISTANCE = 10;

	public int laneCount = 3;
	public int progress = 0;
	public float[] obstacleProbsRoad = new float[] {
		100f, 8f, 4f, 3f, 0f, 1f, 1f, 1f, 2f, 2f, 0.05f
	};
	public float[] obstacleProbsMiddle = new float[] {
		100f, 4f, 0.5f, 2f, 1f, 2f, 1f, 1f, 2f, 2f, 0.05f
	};
	public float[] obstacleProbsWall = new float[] {
		100f, 2f, 4f, 2f, 2f, 0f, 3f, 3f, 2f, 2f, 0.05f
	};

	// elements in order:
	// 0 - empty tile
	// 1 - pole
	// 2 - bike hold
	// 3 - fire
	// 4 - dres
	// 5 - old lady
	// 6 - pissing dog
	// 7 - bin
	// 8 - bag
	// 9 - manhole
	// 10 - ufo sign

	public GameObject[] tilePrefabs;
	public GameObject[] wallPrefabs;
	float lastZ, lastPlayerZ;

	static public LevelBehaviour instance;

	public GameObject obstaclePrefab;
	public AudioSource sndStep;
	Transform lastGeneratedObstacle = null;

	Transform t;

	public List<int[]> obstacles;

	// Use this for initialization
	void Start () {
		
		obstacles = LevelLoader.Load ("level");
		instance = this;
		t = transform;

		for(int i=0;i<10;++i) {
			GenerateNextLine();
		}
		lastPlayerZ = t.position.z;
	}

	public void GenerateNextLine() { // generate next line of tiles and wall

		GameObject obj;
		lastZ += ONE_UNIT_Z;
		int filling = 0;
		for (int i=0;i<laneCount;++i) {
			int tileIndex;

			if (progress < obstacles.Count) {
				tileIndex = obstacles[progress][i];
			} else {
				if (laneCount == 0) {
					tileIndex = RandomFromDistribution.Integer (obstacleProbsRoad);
				} else if (i == laneCount - 1) {
					tileIndex = RandomFromDistribution.Integer (obstacleProbsWall);
				} else {
					tileIndex = RandomFromDistribution.Integer (obstacleProbsMiddle);
				}
				if (tileIndex > 0) {
					filling++;
				}
			}

			// to make it sure that path is not completely blocked
			if (filling == laneCount) {
				tileIndex = 0;
			}

			obj = Instantiate(tilePrefabs[tileIndex]);
			obj.transform.SetParent(t);

			obj.transform.localPosition = new Vector3(i * ONE_UNIT_Z,0,lastZ);
			lastGeneratedObstacle = obj.transform;
		}
		int wallIndex = Random.Range(0,wallPrefabs.Length);
		obj = Instantiate(wallPrefabs[wallIndex]);
		obj.transform.SetParent(t);
		obj.transform.localPosition = new Vector3(0,0,lastZ);

		progress += 1;
		UIBehaviour.instance.distanceMeter = progress / 2;
	}
	
	// Update is called once per frame
	void Update () {
		t.Translate(0,0,-Time.deltaTime * MOVEMENT_SPEED);

		if (lastPlayerZ - t.position.z >= ONE_UNIT_Z) {
			lastPlayerZ -= ONE_UNIT_Z;
			GenerateNextLine();
			//sndStep.Play();
		}
	}
}
