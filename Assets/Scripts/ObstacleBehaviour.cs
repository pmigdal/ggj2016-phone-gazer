﻿using UnityEngine;
using System.Collections;

public class ObstacleBehaviour : MonoBehaviour {
	const float DELETE_Z_POS = -4f;
	Transform t;
	// Use this for initialization
	void Start () {
		t = transform;
	}
	
	// Update is called once per frame
	void Update () {
//		t.Translate(0,0,-Time.deltaTime * LevelBehaviour.MOVEMENT_SPEED);
//
		if (t.position.z < DELETE_Z_POS) {
			DestroyObject(gameObject);
		}
	}
}
