﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NotificationsTabBehaviour : MonoBehaviour {
	const float NOTIFY_CREATE_INTERVAL = 10.0f;
	const float ANIM_SPEED = 5.0f;
	public static NotificationsTabBehaviour instance;

	public GameObject notificationPrefab;
	public AudioSource sndNotify;
	public Text txtNotificationsCount;
	public RectTransform notificationsContainer;
	public ScrollRect scrollRect;
	float lastTime;

	RectTransform t;

	// Use this for initialization
	void Start () {
		instance = this;
		lastTime = Time.time;
		t = transform as RectTransform;
	}
	
	// Update is called once per frame
	void Update () {
		// create new notification every n seconds
		if (!LevelBehaviour.instance.enabled) return;
		if (lastTime + NOTIFY_CREATE_INTERVAL < Time.time) {
			// decrease point for every notification not yet handled
			UIBehaviour.instance.socialMeter -= notificationsContainer.childCount;
			lastTime = Time.time;
			sndNotify.Play();
			GameObject obj = Instantiate(notificationPrefab);
			obj.transform.SetParent(notificationsContainer);
			(obj.transform as RectTransform).anchoredPosition3D = new Vector3(8.5f,-notificationsContainer.rect.height - 8.5f,0);
			(obj.transform as RectTransform).localRotation = Quaternion.identity;
		}

		// shitty animation

		for(int i=0;i<notificationsContainer.childCount;++i) {
			RectTransform child = notificationsContainer.GetChild(i) as RectTransform;
			child.anchoredPosition = new Vector2(8.5f, Mathf.Lerp(child.anchoredPosition.y, -8.5f - i * (141.0f+8.5f), ANIM_SPEED * Time.deltaTime));
		}

		notificationsContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, notificationsContainer.childCount * (141.0f+8.5f) + 2 * 8.5f);
		txtNotificationsCount.text = notificationsContainer.childCount.ToString();
	}
}
