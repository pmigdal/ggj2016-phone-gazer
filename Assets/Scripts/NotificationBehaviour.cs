﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NotificationBehaviour : MonoBehaviour {
	public Sprite[] avatarSprites;
	public Image imgAvatar;
	public Text txtNotification;
	public int valence;
	public Animator anim;
	// Use this for initialization
	void Start () {
		RandomFriend friend = new RandomFriend();
		txtNotification.text = string.Format("{0} sends you invite", friend.name);
		valence = friend.valence;
		imgAvatar.overrideSprite = avatarSprites[Random.Range(0,avatarSprites.Length)];
		anim.SetBool("Visible",true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnAcceptOrRejectClick(bool accept) {
		if (!enabled) return;
		int result = accept ? valence : -valence/2;
		if (result >= 0) UIBehaviour.instance.sndGood.Play(); else UIBehaviour.instance.sndBad.Play();
		UIBehaviour.instance.socialMeter += result;
		enabled = false;
		anim.SetBool("Visible",false);
	}

	public void Destroy() {
		DestroyObject(gameObject);
	}
}
