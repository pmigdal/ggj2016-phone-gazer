﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FeedTabBehaviour : MonoBehaviour {
	public static FeedTabBehaviour instance;
	public GameObject feedPostPrefab;
	public RectTransform postsContainer;
	public ScrollRect scrollRect;
	// Use this for initialization
	void Start () {
		instance = this;
		for(int i=0;i<5; ++i) {
			InstantiateFeedPost();
		}
	}
	
	// Update is called once per frame
	void Update () {
		// check if first post dissapeared
		if (postsContainer.childCount > 0) {
			RectTransform firstPost = postsContainer.GetChild(0) as RectTransform;
			float height = firstPost.rect.height + 8.5f;
			if (!Input.GetMouseButton(0) && postsContainer.anchoredPosition.y > height) {
				scrollRect.StopMovement();

				for(int i=1;i<postsContainer.childCount;++i) {
					RectTransform child = postsContainer.GetChild(i) as RectTransform;
					child.anchoredPosition += new Vector2(0,height);
				}
				postsContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, postsContainer.rect.height - height);
				postsContainer.anchoredPosition -= new Vector2(0,height);
				DestroyObject(firstPost.gameObject);

				InstantiateFeedPost();
			}
		}
	}

	void InstantiateFeedPost() {
		// decrease point for every notification not yet handled
		GameObject obj = Instantiate(feedPostPrefab);
		obj.transform.SetParent(postsContainer);
		RectTransform newChild = (obj.transform as RectTransform);
		newChild.anchoredPosition3D = new Vector3(8.5f,-postsContainer.rect.height - 8.5f,0);
		newChild.localRotation = Quaternion.identity;		
		newChild.localScale = Vector3.one;
		obj.GetComponent<FeedPostBehaviour>().Setup();
		postsContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, postsContainer.rect.height + 8.5f + newChild.rect.height);
	}
}
