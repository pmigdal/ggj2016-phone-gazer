﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FeedPostBehaviour : MonoBehaviour {
	public Sprite[] memeSprites;
	public Sprite[] avatarSprites;
	public Text txtName,txtPost;
	public Image imgAvatar;
	public Image imgMeme;
	public int valence;
	public Button btnLike, btnComment;

	RectTransform t;
	// Use this for initialization
	public void Setup () {
		t = transform as RectTransform;
		int postType = Random.Range(0,4);
		RandomFriend friend = new RandomFriend();
		txtName.text = friend.name;
		if (postType != 0) { // post z tekstem
			txtPost.gameObject.SetActive(true);
			imgMeme.gameObject.SetActive(false);

			RandomPost post = new RandomPost();;
			txtPost.text = post.text;

			t.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 90.95f + txtPost.preferredHeight);
			valence = post.valence;
		} else {
			txtPost.gameObject.SetActive(false);
			imgMeme.gameObject.SetActive(true);
			imgMeme.overrideSprite = memeSprites[Random.Range(0,memeSprites.Length)];
			t.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 90.25f + imgMeme.preferredHeight * imgMeme.rectTransform.rect.width / imgMeme.preferredWidth);
			valence = 1;
		}
		imgAvatar.overrideSprite = avatarSprites[Random.Range(0,avatarSprites.Length)];
	}
	
	public void OnLikeClicked() {
		btnLike.interactable = false;
		btnComment.interactable = false;
		UIBehaviour.instance.socialMeter += valence;
		if (valence >= 0) UIBehaviour.instance.sndGood.Play(); else UIBehaviour.instance.sndBad.Play();
	}

	public void OnCommentClicked() {
		btnLike.interactable = false;
		btnComment.interactable = false;
	}
}
