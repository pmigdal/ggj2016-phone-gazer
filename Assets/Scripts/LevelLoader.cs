﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO; 

public class LevelLoader
{
	static int CharToInt(char c) {
		if (c>='0' && c<='9') return (int)(c-'0');
		if (c>='a' && c<='z') return (int)(c-'a')+10;
		return 0;
	}

	public static List<int[]> Load(string filename)
	{
		string line;
		List<int[]> obstacles = new List<int[]>();
		TextAsset text = Resources.Load(filename) as TextAsset;
		StreamReader theReader = new StreamReader(new MemoryStream(text.bytes));

		//StreamReader theReader = new StreamReader("Assets/Resources/" + filename, Encoding.Default);
		do
		{
			line = theReader.ReadLine();

			if (line != null)
			{
				obstacles.Add(new int[] {
					CharToInt(line[0]),
					CharToInt(line[1]),
					CharToInt(line[2]),
				});
			}
		}
		while (line != null);
		theReader.Close();
		return obstacles;
	}
}

