﻿using UnityEngine;
using System.Collections;

public class RandomFriend
{

	public static string[] nameBad = new string[] {
		"sPAMeR", "Cheap", "Bytch", "Adolf", "Two-faced", "Dummb", "Anoyin",
		"Mean", "Paine", "Lame", "Nobody", "Noone", "Notkool"
	};
	public static string[] nameGood = new string[] {
		"Rich", "Kitty", "Fortuna", "Rose", "Clara", "Leonardo", "Lovely",
		"Sunshine", "Amanda", "Rick", "Moneyla", "Amore", "Popular", "Hipster"
	};
	public static string[] surnameBad = new string[] {
		"Dykhead", "Aswhole", "Creepsky", "Stalin", "Smaldyck",
		"Creepmann", "Creepberg", "Creepson", "Creeper",  "McCreep",
		"inDeAzz"

	};
	public static string[] surnameGood = new string[] {
		"Lovely", "Adorable", "Lolson", "Cuteovsky",
		"Smiley", "Darlingovsky", "Likeable", "Cuteson", "Cuteman", "Cutemann",
		"Cuteovitz", "Adoreson", "Adoremann", "Cuteberg", "Cutestein", "Cuter", "McCute", "DiCutio"
	};
	public static string[] yourBad = new string[] {
		"Mother", "Father", "ex-boyfriend", "ex-girlfriend", "Grama", "Granpa", "Fiend",
		"teacher", "uncle", "aunt", "younger sis", "school nerd", "science fan", "the best pupil", "plodder"
	};
	public static string[] yourGood = new string[] {
		"Crush", "Sweetheart", "Soulmate", "Fckbuddy", "Best Friend", "2nd Best Friend", "Popular Friend",
		"Lovely Friend", "Cute Friend", "Sexy Friend"
	};

	public string name;
	public int valence;
	int rand1;
	int rand2;

	string chooseRandom (string[] names) {
		int rand = Random.Range (0, names.Length);
		return names [rand];
	}

	// two main fields:
	// name (string)
	// valence (how many friends it adds to subtracts)
	public RandomFriend ()
	{
		rand1 = Random.Range (0, 10);

		if (rand1 == 0) {
			valence = -10;
			name = string.Format ("Your {0}", chooseRandom (yourBad));
		} else if (rand2 == 1) {
			valence = 5;
			name = string.Format ("Your {0}", chooseRandom (yourGood));
		} else {
			string name1;
			string name2;

			rand2 = Random.Range (0, 3);
			if (rand2 == 0) {
				valence = -2;
				name1 = chooseRandom (nameBad);
			} else {
				valence = 1;
				name1 = chooseRandom (nameGood);
			}

			rand2 = Random.Range (0, 3);
			if (rand2 == 0) {
				valence += -2;
				name2 = chooseRandom (surnameBad);
			} else {
				valence += 1;
				name2 = chooseRandom (surnameGood);
			}

			name = string.Format ("{0} {1}", name1, name2);

		}
	}
}
	