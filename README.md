# Ritu-blah-blau

* for GlobalGameJam 2016 at PolyJam, Warsaw, Poland
* by:
    * [@nosferathoo](https://twitter.com/nosferathoo) - programming
    * [Paulina Chamczyk](http://crisiummare.blogspot.com) - graphics
    * [@FakePsyho](https://twitter.com/FakePsyho) - game design, music
    * PaLina - meme generation
    * [@pmigdal](https://twitter.com/pmigdal) - git master, random generation   
* website: [globalgamejam.org/2016/games/ritu-blah-blau](http://globalgamejam.org/2016/games/ritu-blah-blau)
* source: [bitbucket.org/pmigdal/ggj2016-phone-gazer](https://bitbucket.org/pmigdal/ggj2016-phone-gazer)
* tech: [Unity](https://unity3d.com/) 5.3.2f1, [Gimp](https://www.gimp.org/)
* **[Play it online!](http://gamejolt.com/games/ritu-blah-blau/123314)**


![](screenshots/title_screen.png)
![](screenshots/obstacles.png)
![](screenshots/phone.png)

Two channels of distraction: the social ritual of adding Gazebook friends vs the mundane task of avoiding obstacles while walking.

You just got your first smartphone and that means you start your ritual of social bonding with your BFFs on Gazebook.

Be careful which invites you accept and what you "like"!
How many friends can you get and how far will you go before you fail @ life?

Controls:

* `A` and `D` - move left or right
* Mouse and `left click` - use your phone
* Move mouse to the bottom of the screen to see where you're walking.
